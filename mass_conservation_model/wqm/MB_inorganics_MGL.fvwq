! INORGANICS SIMULATION CLASS
!_________________________________________________________________
! SIMULATION CONTROLS

simulation class == inorganics
wq dt == 900.0 
wq units == mgl
!_________________________________________________________________
! CONSTITUENT MODEL SETTINGS

oxygen model == O2
	oxygen min max == 0.0, 12.0
	oxygen benthic == 4.7, 1.08
end oxygen model

silicate model == si
	silicate min max == 0.0, 100.0
	silicate benthic == 4.2, 1.01
	oxygen == on
end silicate model

inorganic nitrogen model == ammoniumnitrate
	ammonium min max == 0.0, 50.0
	nitrate min max == 0.0, 50.0
	ammonium benthic == 4.05, 1.06
	nitrate benthic == 4.25, 1.10
	nitrification == 0.05, 4.15, 1.01
	denitrification == michaelis menten, 0.05, 4.01, 1.03
	oxygen == on
	atmospheric deposition == 5.0, 0.0, 0.5
	anaerobic oxidation of ammonium == 0.005, 2.5, 2.5
	diss nitrate reduction to ammonium == 0.005, 5.0
end inorganic nitrogen model

inorganic phosphorus model == frphsads
	FRP min max == 0.0, 5.0
	FRP ads min max == 0.0, 5.0
	FRP benthic == 4.3, 1.095
	oxygen == on
	atmospheric deposition == 0.5, 0.0
	adsorption == linear, 0.25
	settling == 0.000
end inorganic phosphorus model

phyto model == advanced, bluegreen
	min max == 0.05, 50.0
	temperature limitation == standard, 19.0, 22.0, 29.0
	salinity limitation == mixed, 5.0, 10.0, 2.5
	light limitation == chalker, 0.0005, 150.0
	nitrogen limitation == 0.005, 1.5, 3.0, 5.0
	phosphorus limitation == 0.002, 0.12, 0.3, 0.6
	silicate limitation == 0.005, 9.0
	uptake == 2.5, 0.3, 8.0
	primary productivity == 3.4, 1.08 
	respiration == 0.06, 1.04, 0.2, 0.3, 0.1 
	carbon chla ratio == 26.8
	nitrogen fixing == 0.002, 0.3
	settling == constant, -0.3
end phyto model	

phyto model == basic, green
	min max == 0.05, 50.0
	temperature limitation == standard, 19.0, 22.0, 29.0
	salinity limitation == fresh, 2.0, 5.0, 0.0
	light limitation == steele, 0.0005, 150.0
	nitrogen limitation == 0.01, 2.5
	phosphorus limitation == 0.005, 0.05
	silicate limitation == 0.01, 4.0
	uptake == 3.0, 0.6, 8.0
	primary productivity == 2.6, 1.1 
	respiration == 0.005, 1.1, 0.3, 0.4, 0.5 
	carbon chla ratio == 27.8
	nitrogen fixing == 0.001, 0.5
	settling == constant, -0.4
end phyto model

pathogen model == attached, pth1
	alive min max == 0.0, 1e7
	mortality == 0.0008, 0.0, 6.1, 1.0, 1.14
	visible inactivation == 0.0, 0.0667, 0.1  
	uva inactivation == 0.01, 0.00667, 0.1  
	uvb inactivation == 0.02, 0.00667, 0.1  
	settling == -0.001, -0.2
	target attached fraction == 0.5
end pathogen model

pathogen model == free, pth2
	alive min max == 0.0, 1e7
	mortality == 0.08, 2e-12, 6.1, 1.0, 1.11 
	visible inactivation == 0.082, 0.0067, 0.5 
	uva inactivation == 0.5, 0.0067, 0.5 
	uvb inactivation == 1.0, 0.0067, 0.5 
	settling == -0.03
end pathogen model

!_________________________________________________________________
! MATERIAL SPECIFICATIONS

material == default
	oxygen flux ==  -1400.0
	silicate flux == 10.0
	ammonium flux == 20.0
	nitrate flux == 30.0
	frp flux == 5.0
end material

material == 1
	oxygen flux == -1100.0
	silicate flux == 11.0
	ammonium flux == 21.0
	nitrate flux == 31.0
	frp flux == 6.0
end material

material == 2
	oxygen flux == -1200.0
	silicate flux == 12.0
	ammonium flux == 22.0
	nitrate flux == 32.0
	frp flux == 7.0
end material

material == 3
	oxygen flux == -1300.0
	silicate flux == 13.0
	ammonium flux == 23.0
	nitrate flux == 33.0
	frp flux == 8.0
end material
