# WQ mass balance write files and plots

import pandas as pd
import os
import numpy as np
from matplotlib import pyplot as plt


def write_xlsx(flux, mass_bal, nc_in, out_dir, run_ID, simclass=3):
    var_sequence0 = ['oxy_atm_flux', 'oxy_sed_flux', 'oxy_mass_bal',
                     'pat_grw_flux', 'pat_alat_lgt_flux', 'pat_alat_mor_flux',
                     'pat_alat_at_sed_flux', 'pat_alat_al_sed_flux', 'pth_alive_attch_mass_bal',
                     'pat_dead_de_sed_flux', 'pth_dead_mass_bal', 'tpt_mass_bal']
    var_sequence1 = ['oxy_atm_flux', 'oxy_sed_flux', 'oxy_nitrif_flux', 'oxy_phy_gpp_flux', 'oxy_phy_res_flux',
                     'oxy_mass_bal',
                     'pat_grw_flux', 'pat_alat_lgt_flux', 'pat_alat_mor_flux',
                     'pat_alat_at_sed_flux', 'pat_alat_al_sed_flux', 'pth_alive_attch_mass_bal',
                     'pat_dead_de_sed_flux', 'pth_dead_mass_bal',
                     'sil_sed_flux', 'sil_phy_upt_flux', 'sil_phy_mor_flux', 'sil_phy_exc_flux', 'sil_mass_bal',
                     'amm_atm_flux', 'amm_sed_flux', 'amm_nitrif_flux', 'amm_anmmox_flux', 'amm_drna_flux',
                                                                                           'amm_phy_upt_flux',
                     'amm_phy_mor_flux', 'amm_phy_exc_flux', 'amm_mass_bal',
                     'nit_atm_flux', 'nit_sed_flux', 'nit_nitrif_flux', 'nit_denit_flux', 'nit_anmmox_flux',
                     'nit_drna_flux',
                     'nit_phy_upt_flux', 'nit_mass_bal',
                     'frp_atm_flux', 'frp_sed_flux', 'frp_phy_upt_flux', 'frp_phy_mor_flux', 'frp_phy_exc_flux',
                     'frp_mass_bal',
                     'phy_phy_upt_flux', 'phy_phy_res_flux', 'phy_phy_mor_flux', 'phy_phy_exc_flux', 'phy_phy_sed_flux',
                     'phy_mass_bal',
                     'tni_mass_bal', 'tkn_mass_bal', 'tph_mass_bal', 'toc_mass_bal', 'tpt_mass_bal']
    var_sequence3 = [
        'oxy_atm_flux','oxy_sed_flux','oxy_nitrif_flux','oxy_phy_gpp_flux','oxy_phy_res_flux','oxy_org_min_flux','oxy_mass_bal',
        'pat_grw_flux', 'pat_alat_lgt_flux', 'pat_alat_mor_flux',
        'pat_alat_at_sed_flux', 'pat_alat_al_sed_flux', 'pth_alive_attch_mass_bal',
        'pat_dead_de_sed_flux', 'pth_dead_mass_bal',
        'sil_sed_flux','sil_phy_upt_flux','sil_phy_mor_flux','sil_phy_exc_flux','sil_mass_bal',
        'amm_atm_flux','amm_sed_flux','amm_nitrif_flux','amm_anmmox_flux','amm_drna_flux',
        'amm_phy_upt_flux','amm_org_min_flux','amm_org_pho_flux','amm_mass_bal',
        'nit_atm_flux','nit_sed_flux','nit_nitrif_flux','nit_denit_flux','nit_anmmox_flux','nit_drna_flux',
        'nit_phy_upt_flux','nit_org_min_flux','nit_mass_bal',
        'frp_atm_flux','frp_sed_flux','frp_phy_upt_flux','frp_org_min_flux','frp_org_pho_flux','frp_mass_bal',
        'phy_phy_upt_flux','phy_phy_res_flux','phy_phy_mor_flux','phy_phy_exc_flux','phy_phy_sed_flux','phy_mass_bal',
        'poc_hyd_flux','poc_phy_mor_flux','poc_bdn_flux','poc_sed_flux','poc_mass_bal',
        'doc_sed_flux','doc_hyd_flux','doc_min_flux','doc_phy_exc_flux','doc_act_flux','doc_pho_flux','doc_mass_bal',
        'pon_hyd_flux','pon_phy_mor_flux','pon_bdn_flux','pon_sed_flux','pon_mass_bal',
        'don_sed_flux','don_hyd_flux','don_min_flux','don_phy_exc_flux','don_act_flux','don_pho_flux','don_mass_bal',
        'pop_hyd_flux','pop_phy_mor_flux','pop_bdn_flux','pop_sed_flux','pop_mass_bal',
        'dop_sed_flux','dop_hyd_flux','dop_min_flux','dop_phy_exc_flux','dop_act_flux','dop_pho_flux','dop_mass_bal',
        'rpc_bdn_flux','rpc_sed_flux','rpc_mass_bal',
        'rdc_act_flux','rdc_pho_flux','rdc_mass_bal',
        'rdn_act_flux','rdn_pho_flux','rdn_mass_bal',
        'rdp_act_flux','rdp_pho_flux','rdp_mass_bal',
        'tni_mass_bal','tkn_mass_bal','tph_mass_bal','toc_mass_bal', 'tpt_mass_bal']
    if simclass == 0:
        var_sequence = var_sequence0
    elif simclass == 1:
        var_sequence = var_sequence1
    elif simclass == 3:
        var_sequence = var_sequence3
    t_max = max(nc_in['ResTime'])
    t_min = min(nc_in['ResTime'])
    tstep = nc_in['ResTime'][1] - nc_in['ResTime'][0]
    time = np.linspace(0, t_max - t_min, round(1 + (t_max - t_min) / tstep))
    filename = os.path.join(out_dir, run_ID + '_mass_conservation.xlsx')
    if os.path.exists(filename):
        os.remove(filename)
    list_massbal = dir(mass_bal)
    list_flux = dir(flux)
    timedict = {'time': time.flatten().tolist()}
    datadict = {}
    list_massbal_clear = [var for var in list_massbal if not var.startswith('_')]
    list_massbal_clear.remove('adding_new_attr')
    list_flux_clear = [var for var in list_flux if not var.startswith('_')]
    list_flux_clear.remove('adding_new_attr')
    list_flux_clear.remove('get_components')
    for var in var_sequence:  # Output all vars with prescribed sequence
        if var in list_flux_clear:
            datadict.update({var: flux.__getattribute__(var).flatten().tolist()})
        elif var in list_massbal_clear:
            datadict.update({var: mass_bal.__getattribute__(var).flatten().tolist()})
        else:
            pass
    # for var in list_massbal_clear: #Output all mass_balance and flux vars based on names
    # datadict.update({var: mass_bal.__getattribute__(var).flatten().tolist()})
    # for var in list_flux_clear:
    # datadict.update({var: flux.__getattribute__(var).flatten().tolist()})
    for key, value in datadict.items():
        timedict[key] = value

    df = pd.DataFrame(data=timedict)
    df.to_excel(filename, sheet_name='sheet1', index=False)

def plot_mass_balance(nc_in, mass_bal, simclass, run_ID):
    time = nc_in['ResTime'][:] - nc_in['ResTime'][:][0]
    plt.figure(figsize=(12, 9))
    plt.subplot(2, 1, 1)
    plt.plot(time, mass_bal.oxy_mass_bal, label='oxy')
    plt.plot(time, mass_bal.pat_alat_mass_bal, label='alive paths')
    plt.plot(time, mass_bal.pat_dead_mass_bal, label='dead paths')
    plt.xlabel('Simulation Hour')
    plt.ylabel("Mass conservation error (%) ")
    plt.subplots_adjust(left=0.12, bottom=0.10, right=0.97, top=0.95, wspace=0.23, hspace=0.23)
    #plt.subplots_adjust(pad=2, h_pad=1, w_pad=None, rect=None)
    if simclass > 0:
        plt.plot(time, mass_bal.sil_mass_bal, label='sil')
        plt.plot(time, mass_bal.amm_mass_bal, label='amm')
        plt.plot(time, mass_bal.nit_mass_bal, label='nit')
        plt.plot(time, mass_bal.frp_mass_bal, label='frp+frpads')
        plt.plot(time, mass_bal.phy_mass_bal, label='phy')
    if simclass > 1:
        plt.plot(time, mass_bal.poc_mass_bal, label='poc')
        plt.plot(time, mass_bal.doc_mass_bal, label='doc')
        plt.plot(time, mass_bal.pon_mass_bal, label='pon')
        plt.plot(time, mass_bal.don_mass_bal, label='don')
        plt.plot(time, mass_bal.pop_mass_bal, label='pop')
        plt.plot(time, mass_bal.dop_mass_bal, label='dop')
    if simclass > 2:
        plt.plot(time, mass_bal.rpc_mass_bal, label='rpom')
        plt.plot(time, mass_bal.rdc_mass_bal, label='rdoc')
        plt.plot(time, mass_bal.rdn_mass_bal, label='rdon')
        plt.plot(time, mass_bal.rdp_mass_bal, label='rdop')

    # Totals
    plt.subplot(2, 1, 2)
    plt.plot(time, mass_bal.tpt_mass_bal, label='Total path')
    plt.xlabel('Simulation Hour')
    plt.ylabel("Mass conservation error (%) ")

    if simclass > 0:
        plt.plot(time, mass_bal.tni_mass_bal, label='TN')
        plt.plot(time, mass_bal.tph_mass_bal, label='TP')
    if simclass < 2:
        plt.subplot(2, 1, 1).legend(loc="upper center", ncol=len(plt.subplot(2, 1, 1).lines))
    else:
        plt.subplot(2, 1, 1).legend(loc="upper center", ncol=int(0.5*len(plt.subplot(2, 1, 1).lines)))

    plt.subplot(2, 1, 2).legend(loc="upper center", ncol=len(plt.subplot(2, 1, 2).lines))
    plt.subplot(2, 1, 2).set_ylim([-0.1, 0.1])
    plt.subplot(2, 1, 2).grid(color='black', linestyle=':', linewidth=0.25)
    plt.subplot(2, 1, 1).set_ylim([-0.1, 0.1])
    plt.subplot(2, 1, 1).grid(color='black', linestyle=':', linewidth=0.25)
    plt.subplot(2, 1, 1).set_title(run_ID, fontweight="bold")
    plt.show()
