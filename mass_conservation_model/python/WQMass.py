# WQ Mass Balance Calculation Module
import numpy as np


class Flux:
    def __init__(self, ncells):
        zeros = np.zeros([ncells, 1], dtype=np.float32)
        self.oxy_atm_flux = zeros
        self.oxy_sed_flux = zeros
        self.oxy_nitrif_flux = zeros
        self.oxy_phy_gpp_flux = zeros
        self.oxy_phy_res_flux = zeros
        self.oxy_org_min_flux = zeros

    def adding_new_attr(self, attr, val):
        setattr(self, attr, val)

    def get_components(self, key):
        varlist = []
        for var in dir(self):
            if var.startswith(key):
                varlist.append(var)
        return varlist


class Mass:
    def __init__(self, ncells):
        zeros = np.zeros([ncells, 1], dtype=np.float32)
        self.oxy_mass = zeros

    def adding_new_attr(self, attr, val):
        setattr(self, attr, val)


class MassBalance:
    def __init__(self, ncells):
        zeros = np.zeros([ncells, 1], dtype=np.float32)
        self.oxy_mass_bal = zeros
        self.oxy_comp_mass = zeros

    def adding_new_attr(self, attr, val):
        setattr(self, attr, val)


def surf_ids(NL):
    ids = np.zeros([np.size(NL),1], dtype=np.int32)
    ids[0] = NL[0]
    for i in range(1, np.size(NL)):
        ids[i] = ids[i - 1] + NL[i]
    return ids


def sedi_ids(NL):  # these two are the same functions?
    ids = np.zeros([np.size(NL),1], dtype=np.int32)
    ids[0] = NL[0]
    for i in range(1, np.size(NL)):
        ids[i] = ids[i - 1] + NL[i]
    return ids


def cell_dz(NL, zl):
    NL_start = np.zeros([np.size(NL),1], dtype=np.int32)
    NL_end = np.zeros([np.size(NL),1], dtype=np.int32)
    NL_start[0] = 0
    NL_end[0] = NL_start[0] + NL[0]
    for i in range(1, np.size(NL)):
        NL_start[i] = NL_start[i - 1] + NL[i - 1] + 1
        NL_end[i] = NL_start[i] + NL[i]

    dz = np.zeros([np.shape(zl)[0], sum(NL)], dtype=np.float32)
    for k in range(0, np.shape(zl)[0]):
        for i in range(0, np.size(NL)):
            for j in range(int(NL_start[i]), int(NL_end[i]) ):
                dz[k, j - i] = zl[k, j + 1] - zl[k, j]
    dz = -dz
    return dz


def atmosph_flux(flux, NL, area):
    surf_cell_id = surf_ids(NL)
    atm = np.zeros([np.shape(flux)[0], 1], dtype=np.float32)
    for k in range(0, np.shape(flux)[0]):
        for i in range(0, np.size(surf_cell_id)):
            atm[k] = atm[k] + sum(area[i] * flux[k, surf_cell_id[i] - 1])
    return atm


def benthic_flux(fl, NL, area):
    benth_cell_id = sedi_ids(NL)
    #flux = fl.sum(axis=2)  # check sum functions in matlab and python!
    try:
        flux = fl.sum(axis=2)
    except np.AxisError:
        flux = fl
    sed = np.zeros([np.shape(flux)[0], 1], dtype=np.float32)
    for k in range(0, np.shape(flux)[0]):
        for i in range(0, np.size(benth_cell_id)):
            sed[k] = sed[k] + sum(area[i] * flux[k, benth_cell_id[i] - 1])
    return sed


def volume_flux(fl, NL, zl, area):
    dzs = cell_dz(NL, zl)
    try:
        flux = fl.sum(axis=2)
    except np.AxisError:
        flux = fl
    vf = np.zeros([np.shape(flux)[0], 1], dtype=np.float32)
    for j in range(0, np.shape(flux)[0]):
        for i in range(0, np.size(NL)):
            start = sum(NL[0:i])  # check index
            for k in range(start, start + NL[i]):
                vf[j] = vf[j] + (area[i] * (flux[j, k] * dzs[j, k]))
    return vf


def mass_int(con, NL, zl, area):
    dzs = cell_dz(NL, zl)
    try:
        conc = con.sum(axis=2)
    except np.AxisError:
        conc = con
    m = np.zeros([np.shape(conc)[0], 1], dtype=np.float32)
    for j in range(0, np.shape(conc)[0]):
        for i in range(0, np.size(NL)):
            start = sum(NL[0:i])
            for k in range(start, start + NL[i]):
                m[j] = m[j] + (area[i] * (conc[j, k] * dzs[j, k]))
    return m


def mass_balance(mass, flux, tol, var):
    if var[0].startswith('nit'):
        var.remove('nit_phy_fix_flux')
    m = np.zeros([np.size(mass), 1], dtype=np.float32)
    m[0:2] = mass[0]
    for i in range(2, np.size(mass)):
        flux_sum = 0
        for j in range(0, int(np.shape(var)[0])):
            flux_sum = flux_sum + flux.__getattribute__(var[j])[i-1]
        m[i] = m[i-1] + flux_sum
    mb = (mass - m) * 100 / mass
    mb[np.where(mass < tol * mass[0])] = np.nan
    return mb, m


def compute_fluxes(nc_in, WQDiag, WQVar, num_pth, glb, NL, area, zfaces):
    ncell = np.shape(WQDiag.oxy_atm)[0]
    flux = Flux(ncell)
    mass = Mass(ncell)
    flux.oxy_atm_flux = atmosph_flux(WQDiag.oxy_atm, NL, area) * glb.conv_area
    flux.oxy_sed_flux = benthic_flux(WQDiag.oxy_sed, NL, area) * glb.conv_area
    flux.oxy_nitrif_flux = volume_flux(WQDiag.oxy_nit, NL, zfaces, area) * glb.conv_volu
    flux.oxy_phy_gpp_flux = volume_flux(WQDiag.oxy_gpp, NL, zfaces, area) * glb.conv_volu
    flux.oxy_phy_res_flux = volume_flux(WQDiag.oxy_rsf, NL, zfaces, area) * glb.conv_volu
    flux.oxy_org_min_flux = volume_flux(WQDiag.oxy_omi, NL, zfaces, area) * glb.conv_volu
    mass.oxy_mass = mass_int(WQVar.oxy, NL, zfaces, area) * glb.conv_mass
    simclass = glb.sim_class

    mass.pat_ali_mass = mass_int(WQVar.pth_ali, NL, zfaces, area) * glb.conv_mass_CFU
    mass.pat_dead_mass = mass_int(WQVar.pth_dea, NL, zfaces, area) * glb.conv_mass_CFU
    mass.pat_att_mass = mass_int(WQVar.pth_att, NL, zfaces, area) * glb.conv_mass_CFU
    mass.pat_alat_mass = mass.pat_ali_mass + mass.pat_att_mass
    flux.pat_alat_grw_flux = volume_flux(WQDiag.pth_grw, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_alat_lgt_flux = volume_flux(WQDiag.pth_lgt_al, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_alat_mor_flux = volume_flux(WQDiag.pth_mor_al, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_dead_lgt_flux = volume_flux(WQDiag.pth_lgt_de, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_dead_mor_flux = volume_flux(WQDiag.pth_mor_de, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_atm_flux = volume_flux(WQDiag.pth_atm, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_alat_al_sed_flux = benthic_flux(WQDiag.pth_als, NL, area) * glb.conv_area_CFU
    flux.pat_dead_de_sed_flux = benthic_flux(WQDiag.pth_des, NL, area) * glb.conv_area_CFU
    flux.pat_alat_at_sed_flux = benthic_flux(WQDiag.pth_ats, NL, area) * glb.conv_area_CFU

    if simclass > 0:
        flux.adding_new_attr('sil_sed_flux', benthic_flux(WQDiag.sil_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('sil_phy_upt_flux', volume_flux(WQDiag.sil_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('sil_phy_mor_flux', volume_flux(WQDiag.sil_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('sil_phy_exc_flux', volume_flux(WQDiag.sil_exc, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('sil_mass', mass_int(WQVar.sil, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('amm_atm_flux', atmosph_flux(WQDiag.amm_atm, NL, area) * glb.conv_area)
        flux.adding_new_attr('amm_sed_flux', benthic_flux(WQDiag.amm_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('amm_nitrif_flux', volume_flux(WQDiag.amm_nit, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_anmmox_flux', volume_flux(WQDiag.amm_amx, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_drna_flux', volume_flux(WQDiag.amm_drn, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_phy_upt_flux', volume_flux(WQDiag.amm_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_phy_mor_flux', volume_flux(WQDiag.amm_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_phy_exc_flux', volume_flux(WQDiag.amm_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_org_min_flux', volume_flux(WQDiag.amm_omi, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_org_pho_flux', volume_flux(WQDiag.amm_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('amm_mass', mass_int(WQVar.amm, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('nit_atm_flux', atmosph_flux(WQDiag.nit_atm, NL, area) * glb.conv_area)
        flux.adding_new_attr('nit_sed_flux', benthic_flux(WQDiag.nit_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('nit_nitrif_flux', volume_flux(WQDiag.nit_nit, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_anmmox_flux', volume_flux(WQDiag.nit_amx, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_drna_flux', volume_flux(WQDiag.nit_drn, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_denit_flux', volume_flux(WQDiag.nit_den, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_phy_upt_flux', volume_flux(WQDiag.nit_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_org_min_flux', volume_flux(WQDiag.nit_omi, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_phy_fix_flux', volume_flux(WQDiag.n2g_upt, NL, zfaces,
                                                             area) * glb.conv_volu)  # warning:this one does not contain nit!
        mass.adding_new_attr('nit_mass', mass_int(WQVar.nit, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('frp_atm_flux', atmosph_flux(WQDiag.frp_atm, NL, area) * glb.conv_area)
        flux.adding_new_attr('frp_sed_flux', benthic_flux(WQDiag.frp_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('frp_phy_upt_flux', volume_flux(WQDiag.frp_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_phy_mor_flux', volume_flux(WQDiag.frp_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_phy_exc_flux', volume_flux(WQDiag.frp_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_org_min_flux', volume_flux(WQDiag.frp_omi, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_org_pho_flux', volume_flux(WQDiag.frp_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('frp_mass', mass_int(WQVar.frp + WQVar.fra, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('phy_phy_sed_flux', benthic_flux(WQDiag.phc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('ntn_phy_sed_flux', benthic_flux(WQDiag.phn_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('phs_phy_sed_flux', benthic_flux(WQDiag.php_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('phy_phy_upt_flux', volume_flux(WQDiag.car_gpp, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('phy_phy_res_flux', volume_flux(WQDiag.car_rsf, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('phy_phy_mor_flux', volume_flux(WQDiag.car_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('phy_phy_exc_flux', volume_flux(WQDiag.car_exc, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('phy_mass', mass_int(WQVar.phy, NL, zfaces, area) * glb.conv_mass)

        zeros = np.zeros([ncell, 1], dtype=np.float32)
        flux.adding_new_attr('doc_sed_flux', zeros)
        flux.adding_new_attr('don_sed_flux', zeros)
        flux.adding_new_attr('dop_sed_flux', zeros)
        flux.adding_new_attr('poc_bdn_flux', zeros)
        flux.adding_new_attr('pon_bdn_flux', zeros)
        flux.adding_new_attr('pop_bdn_flux', zeros)
        flux.adding_new_attr('poc_sed_flux', zeros)
        flux.adding_new_attr('pon_sed_flux', zeros)
        flux.adding_new_attr('pop_sed_flux', zeros)

    # Labile organics
    if simclass > 1:
        flux.adding_new_attr('doc_sed_flux', benthic_flux(WQDiag.doc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('doc_phy_exc_flux', volume_flux(WQDiag.doc_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_hyd_flux', volume_flux(WQDiag.doc_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_min_flux', volume_flux(WQDiag.doc_min, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_act_flux', volume_flux(WQDiag.doc_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_pho_flux', volume_flux(WQDiag.doc_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('doc_mass', mass_int(WQVar.doc, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('poc_sed_flux', benthic_flux(WQDiag.poc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('poc_phy_mor_flux', volume_flux(WQDiag.poc_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('poc_hyd_flux', volume_flux(WQDiag.poc_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('poc_bdn_flux', volume_flux(WQDiag.poc_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('poc_mass', mass_int(WQVar.poc, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('don_sed_flux', benthic_flux(WQDiag.don_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('don_phy_exc_flux', volume_flux(WQDiag.don_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_hyd_flux', volume_flux(WQDiag.don_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_min_flux', volume_flux(WQDiag.don_min, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_act_flux', volume_flux(WQDiag.don_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_pho_flux', volume_flux(WQDiag.don_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('don_mass', mass_int(WQVar.don, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('pon_sed_flux', benthic_flux(WQDiag.pon_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('pon_phy_mor_flux', volume_flux(WQDiag.pon_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pon_hyd_flux', volume_flux(WQDiag.pon_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pon_bdn_flux', volume_flux(WQDiag.pon_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('pon_mass', mass_int(WQVar.pon, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('dop_sed_flux', benthic_flux(WQDiag.dop_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('dop_phy_exc_flux', volume_flux(WQDiag.dop_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_hyd_flux', volume_flux(WQDiag.dop_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_min_flux', volume_flux(WQDiag.dop_min, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_act_flux', volume_flux(WQDiag.dop_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_pho_flux', volume_flux(WQDiag.dop_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('dop_mass', mass_int(WQVar.dop, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('pop_sed_flux', benthic_flux(WQDiag.pop_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('pop_phy_mor_flux', volume_flux(WQDiag.pop_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pop_hyd_flux', volume_flux(WQDiag.pop_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pop_bdn_flux', volume_flux(WQDiag.pop_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('pop_mass', mass_int(WQVar.pop, NL, zfaces, area) * glb.conv_mass)

    # Refractory organics
    if simclass > 2:
        flux.adding_new_attr('rdc_act_flux', volume_flux(WQDiag.rdc_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('rdc_pho_flux', volume_flux(WQDiag.rdc_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rdc_mass', mass_int(WQVar.rdc, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('rdn_act_flux', volume_flux(WQDiag.rdn_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('rdn_pho_flux', volume_flux(WQDiag.rdn_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rdn_mass', mass_int(WQVar.rdn, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('rdp_act_flux', volume_flux(WQDiag.rdp_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('rdp_pho_flux', volume_flux(WQDiag.rdp_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rdp_mass', mass_int(WQVar.rdp, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('rpc_sed_flux', benthic_flux(WQDiag.rpc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('rpc_bdn_flux', volume_flux(WQDiag.rpc_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rpc_mass', mass_int(WQVar.rpc, NL, zfaces, area) * glb.conv_mass)

    #  Totals
    #  Pathogens
    tpt_mass = mass_int(WQDiag.pth_tot, NL, zfaces, area) * glb.conv_mass_CFU \
               - np.cumsum(flux.pat_alat_al_sed_flux + flux.pat_dead_de_sed_flux + \
                           flux.pat_alat_at_sed_flux).reshape(ncell, 1)
    mass.adding_new_attr('tpt_mass', tpt_mass)

    if simclass > 0:
        tni_mass = mass_int(nc_in['WQ_DIAG_TOTAL_NITROGEN_MG_L'][:], NL, zfaces, area) * glb.conv_mass \
                   - np.cumsum(flux.amm_sed_flux
                               + flux.amm_atm_flux
                               + flux.nit_sed_flux
                               + flux.nit_atm_flux
                               + flux.nit_denit_flux
                               + flux.amm_anmmox_flux
                               + flux.nit_anmmox_flux
                               + flux.nit_phy_fix_flux
                               + flux.ntn_phy_sed_flux
                               + flux.nit_org_min_flux
                               + flux.don_sed_flux
                               + flux.pon_bdn_flux
                               + flux.pon_sed_flux).reshape(ncell, 1)

        tkn_mass = mass_int(nc_in['WQ_DIAG_TOTAL_KJELDAHL_NITROGEN_MG_L'][:], NL, zfaces, area) * glb.conv_mass \
                   - np.cumsum(flux.amm_sed_flux
                               + flux.amm_atm_flux
                               + flux.amm_nitrif_flux
                               + flux.amm_anmmox_flux
                               + flux.ntn_phy_sed_flux
                               + flux.don_sed_flux
                               + flux.pon_bdn_flux
                               + flux.pon_sed_flux).reshape(ncell, 1)

        tph_mass = mass_int(nc_in['WQ_DIAG_TOTAL_PHOSPHORUS_MG_L'][:], NL, zfaces, area) * glb.conv_mass \
                   - np.cumsum(flux.frp_sed_flux
                               + flux.frp_atm_flux
                               + flux.phs_phy_sed_flux
                               + flux.dop_sed_flux
                               + flux.pop_bdn_flux
                               + flux.pop_sed_flux).reshape(ncell, 1)

        toc_mass = mass_int(nc_in['WQ_DIAG_TOTAL_ORGANIC_CARBON_MG_L'][:], NL, zfaces, area) * glb.conv_mass

        mass.adding_new_attr('tni_mass', tni_mass)
        mass.adding_new_attr('tkn_mass', tkn_mass)
        mass.adding_new_attr('tph_mass', tph_mass)
        mass.adding_new_attr('toc_mass', toc_mass)
    return flux, mass


def compute_fluxes_mmm(nc_in, WQDiag, WQVar, num_pth, glb, NL, area, zfaces):
    L_per_m3 = glb.L_per_m3
    N_mm_mg = glb.N_mm_mg
    P_mm_mg = glb.P_mm_mg
    C_mm_mg = glb.C_mm_mg
    ncell = np.shape(WQDiag.oxy_atm)[0]
    flux = Flux(ncell)
    mass = Mass(ncell)
    flux.oxy_atm_flux = atmosph_flux(WQDiag.oxy_atm, NL, area) * glb.conv_area
    flux.oxy_sed_flux = benthic_flux(WQDiag.oxy_sed, NL, area) * glb.conv_area
    flux.oxy_nitrif_flux = volume_flux(WQDiag.oxy_nit, NL, zfaces, area) * glb.conv_volu
    flux.oxy_phy_gpp_flux = volume_flux(WQDiag.oxy_gpp, NL, zfaces, area) * glb.conv_volu
    flux.oxy_phy_res_flux = volume_flux(WQDiag.oxy_rsf, NL, zfaces, area) * glb.conv_volu
    flux.oxy_org_min_flux = volume_flux(WQDiag.oxy_omi, NL, zfaces, area) * glb.conv_volu
    mass.oxy_mass = mass_int(WQVar.oxy, NL, zfaces, area) * glb.conv_mass
    simclass = glb.sim_class

    mass.pat_ali_mass = mass_int(WQVar.pth_ali, NL, zfaces, area) * glb.conv_mass_CFU
    mass.pat_dead_mass = mass_int(WQVar.pth_dea, NL, zfaces, area) * glb.conv_mass_CFU
    mass.pat_att_mass = mass_int(WQVar.pth_att, NL, zfaces, area) * glb.conv_mass_CFU
    mass.pat_alat_mass = mass.pat_ali_mass + mass.pat_att_mass
    flux.pat_alat_grw_flux = volume_flux(WQDiag.pth_grw, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_alat_lgt_flux = volume_flux(WQDiag.pth_lgt_al, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_alat_mor_flux = volume_flux(WQDiag.pth_mor_al, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_dead_lgt_flux = volume_flux(WQDiag.pth_lgt_de, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_dead_mor_flux = volume_flux(WQDiag.pth_mor_de, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_atm_flux = volume_flux(WQDiag.pth_atm, NL, zfaces, area) * glb.conv_volu_CFU
    flux.pat_alat_al_sed_flux = benthic_flux(WQDiag.pth_als, NL, area) * glb.conv_area_CFU
    flux.pat_dead_de_sed_flux = benthic_flux(WQDiag.pth_des, NL, area) * glb.conv_area_CFU
    flux.pat_alat_at_sed_flux = benthic_flux(WQDiag.pth_ats, NL, area) * glb.conv_area_CFU

    if simclass > 0:
        flux.adding_new_attr('sil_sed_flux', benthic_flux(WQDiag.sil_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('sil_phy_upt_flux', volume_flux(WQDiag.sil_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('sil_phy_mor_flux', volume_flux(WQDiag.sil_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('sil_phy_exc_flux', volume_flux(WQDiag.sil_exc, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('sil_mass', mass_int(WQVar.sil, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('amm_atm_flux', atmosph_flux(WQDiag.amm_atm, NL, area) * glb.conv_area)
        flux.adding_new_attr('amm_sed_flux', benthic_flux(WQDiag.amm_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('amm_nitrif_flux', volume_flux(WQDiag.amm_nit, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_anmmox_flux', volume_flux(WQDiag.amm_amx, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_drna_flux', volume_flux(WQDiag.amm_drn, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_phy_upt_flux', volume_flux(WQDiag.amm_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_phy_mor_flux', volume_flux(WQDiag.amm_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_phy_exc_flux', volume_flux(WQDiag.amm_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_org_min_flux', volume_flux(WQDiag.amm_omi, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('amm_org_pho_flux', volume_flux(WQDiag.amm_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('amm_mass', mass_int(WQVar.amm, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('nit_atm_flux', atmosph_flux(WQDiag.nit_atm, NL, area) * glb.conv_area)
        flux.adding_new_attr('nit_sed_flux', benthic_flux(WQDiag.nit_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('nit_nitrif_flux', volume_flux(WQDiag.nit_nit, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_anmmox_flux', volume_flux(WQDiag.nit_amx, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_drna_flux', volume_flux(WQDiag.nit_drn, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_denit_flux', volume_flux(WQDiag.nit_den, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_phy_upt_flux', volume_flux(WQDiag.nit_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_org_min_flux', volume_flux(WQDiag.nit_omi, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('nit_phy_fix_flux', volume_flux(WQDiag.n2g_upt, NL, zfaces,
                                                             area) * glb.conv_volu)  # warning:this one does not contain nit!
        mass.adding_new_attr('nit_mass', mass_int(WQVar.nit, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('frp_atm_flux', atmosph_flux(WQDiag.frp_atm, NL, area) * glb.conv_area)
        flux.adding_new_attr('frp_sed_flux', benthic_flux(WQDiag.frp_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('frp_phy_upt_flux', volume_flux(WQDiag.frp_upt, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_phy_mor_flux', volume_flux(WQDiag.frp_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_phy_exc_flux', volume_flux(WQDiag.frp_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_org_min_flux', volume_flux(WQDiag.frp_omi, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('frp_org_pho_flux', volume_flux(WQDiag.frp_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('frp_mass', mass_int(WQVar.frp + WQVar.fra, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('phy_phy_sed_flux', benthic_flux(WQDiag.phc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('ntn_phy_sed_flux', benthic_flux(WQDiag.phn_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('phs_phy_sed_flux', benthic_flux(WQDiag.php_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('phy_phy_upt_flux', volume_flux(WQDiag.car_gpp, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('phy_phy_res_flux', volume_flux(WQDiag.car_rsf, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('phy_phy_mor_flux', volume_flux(WQDiag.car_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('phy_phy_exc_flux', volume_flux(WQDiag.car_exc, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('phy_mass', mass_int(WQVar.phy, NL, zfaces, area) * glb.conv_mass)

        zeros = np.zeros([ncell, 1], dtype=np.float32)
        flux.adding_new_attr('doc_sed_flux', zeros)
        flux.adding_new_attr('don_sed_flux', zeros)
        flux.adding_new_attr('dop_sed_flux', zeros)
        flux.adding_new_attr('poc_bdn_flux', zeros)
        flux.adding_new_attr('pon_bdn_flux', zeros)
        flux.adding_new_attr('pop_bdn_flux', zeros)
        flux.adding_new_attr('poc_sed_flux', zeros)
        flux.adding_new_attr('pon_sed_flux', zeros)
        flux.adding_new_attr('pop_sed_flux', zeros)

    # Labile organics
    if simclass > 1:
        flux.adding_new_attr('doc_sed_flux', benthic_flux(WQDiag.doc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('doc_phy_exc_flux', volume_flux(WQDiag.doc_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_hyd_flux', volume_flux(WQDiag.doc_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_min_flux', volume_flux(WQDiag.doc_min, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_act_flux', volume_flux(WQDiag.doc_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('doc_pho_flux', volume_flux(WQDiag.doc_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('doc_mass', mass_int(WQVar.doc, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('poc_sed_flux', benthic_flux(WQDiag.poc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('poc_phy_mor_flux', volume_flux(WQDiag.poc_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('poc_hyd_flux', volume_flux(WQDiag.poc_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('poc_bdn_flux', volume_flux(WQDiag.poc_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('poc_mass', mass_int(WQVar.poc, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('don_sed_flux', benthic_flux(WQDiag.don_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('don_phy_exc_flux', volume_flux(WQDiag.don_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_hyd_flux', volume_flux(WQDiag.don_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_min_flux', volume_flux(WQDiag.don_min, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_act_flux', volume_flux(WQDiag.don_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('don_pho_flux', volume_flux(WQDiag.don_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('don_mass', mass_int(WQVar.don, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('pon_sed_flux', benthic_flux(WQDiag.pon_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('pon_phy_mor_flux', volume_flux(WQDiag.pon_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pon_hyd_flux', volume_flux(WQDiag.pon_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pon_bdn_flux', volume_flux(WQDiag.pon_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('pon_mass', mass_int(WQVar.pon, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('dop_sed_flux', benthic_flux(WQDiag.dop_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('dop_phy_exc_flux', volume_flux(WQDiag.dop_exc, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_hyd_flux', volume_flux(WQDiag.dop_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_min_flux', volume_flux(WQDiag.dop_min, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_act_flux', volume_flux(WQDiag.dop_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('dop_pho_flux', volume_flux(WQDiag.dop_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('dop_mass', mass_int(WQVar.dop, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('pop_sed_flux', benthic_flux(WQDiag.pop_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('pop_phy_mor_flux', volume_flux(WQDiag.pop_mor, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pop_hyd_flux', volume_flux(WQDiag.pop_hyd, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('pop_bdn_flux', volume_flux(WQDiag.pop_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('pop_mass', mass_int(WQVar.pop, NL, zfaces, area) * glb.conv_mass)

    # Refractory organics
    if simclass > 2:
        flux.adding_new_attr('rdc_act_flux', volume_flux(WQDiag.rdc_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('rdc_pho_flux', volume_flux(WQDiag.rdc_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rdc_mass', mass_int(WQVar.rdc, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('rdn_act_flux', volume_flux(WQDiag.rdn_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('rdn_pho_flux', volume_flux(WQDiag.rdn_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rdn_mass', mass_int(WQVar.rdn, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('rdp_act_flux', volume_flux(WQDiag.rdp_act, NL, zfaces, area) * glb.conv_volu)
        flux.adding_new_attr('rdp_pho_flux', volume_flux(WQDiag.rdp_pho, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rdp_mass', mass_int(WQVar.rdp, NL, zfaces, area) * glb.conv_mass)

        flux.adding_new_attr('rpc_sed_flux', benthic_flux(WQDiag.rpc_sed, NL, area) * glb.conv_area)
        flux.adding_new_attr('rpc_bdn_flux', volume_flux(WQDiag.rpc_bdn, NL, zfaces, area) * glb.conv_volu)
        mass.adding_new_attr('rpc_mass', mass_int(WQVar.rpc, NL, zfaces, area) * glb.conv_mass)

    #  Totals
    #  Pathogens
    tpt_mass = mass_int(WQDiag.pth_tot, NL, zfaces, area) * glb.conv_mass_CFU \
               - np.cumsum(flux.pat_alat_al_sed_flux + flux.pat_dead_de_sed_flux + \
                           flux.pat_alat_at_sed_flux).reshape(ncell, 1)
    mass.adding_new_attr('tpt_mass', tpt_mass)

    if simclass > 0:
        tni_mass = mass_int(nc_in['WQ_DIAG_TOTAL_NITROGEN_MM_M3'][:]/L_per_m3*N_mm_mg, NL, zfaces, area) * glb.conv_mass \
                   - np.cumsum(flux.amm_sed_flux
                               + flux.amm_atm_flux
                               + flux.nit_sed_flux
                               + flux.nit_atm_flux
                               + flux.nit_denit_flux
                               + flux.amm_anmmox_flux
                               + flux.nit_anmmox_flux
                               + flux.nit_phy_fix_flux
                               + flux.ntn_phy_sed_flux
                               + flux.nit_org_min_flux
                               + flux.don_sed_flux
                               + flux.pon_bdn_flux
                               + flux.pon_sed_flux).reshape(ncell, 1)

        tkn_mass = mass_int(nc_in['WQ_DIAG_TOTAL_KJELDAHL_NITROGEN_MM_M3'][:]/L_per_m3*N_mm_mg, NL, zfaces, area) * glb.conv_mass \
                   - np.cumsum(flux.amm_sed_flux
                               + flux.amm_atm_flux
                               + flux.amm_nitrif_flux
                               + flux.amm_anmmox_flux
                               + flux.ntn_phy_sed_flux
                               + flux.don_sed_flux
                               + flux.pon_bdn_flux
                               + flux.pon_sed_flux).reshape(ncell, 1)

        tph_mass = mass_int(nc_in['WQ_DIAG_TOTAL_PHOSPHORUS_MM_M3'][:]/L_per_m3*P_mm_mg, NL, zfaces, area) * glb.conv_mass \
                   - np.cumsum(flux.frp_sed_flux
                               + flux.frp_atm_flux
                               + flux.phs_phy_sed_flux
                               + flux.dop_sed_flux
                               + flux.pop_bdn_flux
                               + flux.pop_sed_flux).reshape(ncell, 1)

        toc_mass = mass_int(nc_in['WQ_DIAG_TOTAL_ORGANIC_CARBON_MM_M3'][:]/L_per_m3*C_mm_mg, NL, zfaces, area) * glb.conv_mass

        mass.adding_new_attr('tni_mass', tni_mass)
        mass.adding_new_attr('tkn_mass', tkn_mass)
        mass.adding_new_attr('tph_mass', tph_mass)
        mass.adding_new_attr('toc_mass', toc_mass)
    return flux, mass


def do_mass_balance(mass, flux, tol, simclass):
    ncells = np.size(mass.oxy_mass)
    mass_bal = MassBalance(ncells)
    var = flux.get_components('oxy')
    oxy_mass_bal, oxy_comp_mass = mass_balance(mass.oxy_mass, flux, tol, var)
    mass_bal.oxy_mass_bal = oxy_mass_bal
    mass_bal.oxy_comp_mass = oxy_comp_mass

    var = flux.get_components('pat_alat')
    pat_alat_mass_bal, pat_alat_comp_mass = mass_balance(mass.pat_alat_mass, flux, tol, var)
    mass_bal.pat_alat_mass_bal = pat_alat_mass_bal
    mass_bal.pat_alat_comp_mass = pat_alat_comp_mass
    var = flux.get_components('pat_dead')
    pat_dead_mass_bal, pat_dead_comp_mass = mass_balance(mass.pat_dead_mass, flux, tol, var)
    mass_bal.pat_dead_mass_bal = pat_dead_mass_bal
    mass_bal.pat_dead_comp_mass = pat_dead_comp_mass

    if simclass > 0:
        namelist = ['sil', 'amm', 'nit', 'frp', 'phy_phy']
        for name in namelist:
            var = flux.get_components(name)
            if name == 'phy_phy':
                name = 'phy'
            name_bal = name + '_mass_bal'
            name_comp = name + '_comp_mass'
            mb, comp_mass = mass_balance(mass.__getattribute__(name+'_mass'), flux, tol, var)
            mass_bal.adding_new_attr(name_bal, mb)
            mass_bal.adding_new_attr(name_comp, comp_mass)

    if simclass > 1:
        namelist = ['doc', 'poc', 'don', 'pon', 'dop', 'pop']
        for name in namelist:
            var = flux.get_components(name)
            name_bal = name + '_mass_bal'
            name_comp = name + '_comp_mass'
            mb, comp_mass = mass_balance(mass.__getattribute__(name + '_mass'), flux, tol, var)
            mass_bal.adding_new_attr(name_bal, mb)
            mass_bal.adding_new_attr(name_comp, comp_mass)

    if simclass > 2:
        namelist = ['rpc', 'rdc', 'rdn', 'rdp']
        for name in namelist:
            var = flux.get_components(name)
            name_bal = name + '_mass_bal'
            name_comp = name + '_comp_mass'
            mb, comp_mass = mass_balance(mass.__getattribute__(name + '_mass'), flux, tol, var)
            mass_bal.adding_new_attr(name_bal, mb)
            mass_bal.adding_new_attr(name_comp, comp_mass)
    # Totals
    # Pathogens
    tpt_mass_bal = np.zeros([ncells, 1], dtype=np.float32)
    tpt_mass_bal[0: ncells - 2] = (mass.tpt_mass[1:ncells - 1] - mass.tpt_mass[1]) * 100.0 / mass.tpt_mass[1]
    tpt_mass_bal[-1] = tpt_mass_bal[ncells - 3]
    tpt_mass_bal[-2] = tpt_mass_bal[ncells - 3]
    mass_bal.adding_new_attr('tpt_mass_bal', tpt_mass_bal)

    if simclass > 0:
        tni_mass_bal = np.zeros([ncells, 1], dtype=np.float32)
        tkn_mass_bal = np.zeros([ncells, 1], dtype=np.float32)
        tph_mass_bal = np.zeros([ncells, 1], dtype=np.float32)
        toc_mass_bal = np.zeros([ncells, 1], dtype=np.float32)
        tni_mass_bal[0: ncells - 2] = (mass.tni_mass[1:ncells - 1] - mass.tni_mass[1]) * 100.0 / mass.tni_mass[1]
        tni_mass_bal[-1] = tni_mass_bal[ncells - 3]
        tni_mass_bal[-2] = tni_mass_bal[ncells - 3]
        tkn_mass_bal[0: ncells - 2] = (mass.tkn_mass[1:ncells - 1] - mass.tkn_mass[1]) * 100.0 / mass.tkn_mass[1]
        tkn_mass_bal[-1] = tkn_mass_bal[ncells - 3]
        tkn_mass_bal[-2] = tkn_mass_bal[ncells - 3]
        tph_mass_bal[0: ncells - 2] = (mass.tph_mass[1:ncells - 1] - mass.tph_mass[1]) * 100.0 / mass.tph_mass[1]
        tph_mass_bal[-1] = tph_mass_bal[ncells - 3]
        tph_mass_bal[-2] = tph_mass_bal[ncells - 3]
        toc_mass_bal[0: ncells - 2] = (mass.toc_mass[1:ncells - 1] - mass.toc_mass[1]) * 100.0 / mass.toc_mass[1]
        toc_mass_bal[-1] = toc_mass_bal[ncells - 3]
        toc_mass_bal[-2] = toc_mass_bal[ncells - 3]
        mass_bal.adding_new_attr('tni_mass_bal', tni_mass_bal)
        mass_bal.adding_new_attr('tkn_mass_bal', tkn_mass_bal)
        mass_bal.adding_new_attr('tph_mass_bal', tph_mass_bal)
        mass_bal.adding_new_attr('toc_mass_bal', toc_mass_bal)
    return mass_bal

