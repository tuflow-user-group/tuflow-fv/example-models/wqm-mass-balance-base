%% Wiki: Use of Diagnostics
% An example matlab script to post process phytoplankton 
% concentrations in the mass conservation model and assess
% limitation functions.
% This script is not intended for use other than directly with 
% the relevant wiki page discussion. The script should not be 
% altered in any way. Changes will not be supported
% TUFLOW 2023

clear all; close all;

%% User inputs
threeD_ID = 1; % 3D cell ID
threeD_ID_atm = 20; % Corresponding surface cell 3D ID
ylims = [0.0,40.0]; ylims_neg = [-20.0,0]; ylims_lim_fun = [-0.2,1.2]; % ylimits for plotting
% Name an output file to process
% Change this manually as new results files with different WQ 
% parameterisations (described in the wiki page) are generated
 wq = netcdf_get_var('..\..\results\MB_inorganics_MGL_WQ.nc'); % Base case WQ output file
% Uncomment from line 120 onwards if temperature limitation figures are to
% be plotted

% Do not change anything below here
%% Processing
% Conversions
m3_2_L = 1000.0; micg_2_kg = 1000000000.0;

% Geometry
area = wq.cell_A;
thick = cell_dz(wq.NL,wq.layerface_Z);
for j = 1:length(wq.ResTime)
    for i = 1:length(wq.idx3)
        vol(wq.idx3(i):wq.idx3(i)+wq.NL(i)-1,j) = m3_2_L * thick(wq.idx3(i):wq.idx3(i)+wq.NL(i)-1,j) * area(i); %L
        are(wq.idx3(i):wq.idx3(i)+wq.NL(i)-1,j) = area(i); %m2
    end
end

% Time
wq.ResTime = wq.ResTime - wq.ResTime(1);

% Phytoplankton concentration
phy = wq.WQ_PHYTO_GREEN_CONC_MICG_L;

% Mass sinks converted to kg/d
settl = wq.WQ_DIAG_PHYTO_GREEN_SEDMTN_MICG_L_D.*are/micg_2_kg;
excre = wq.WQ_DIAG_PHYTO_GREEN_EXCR_MICG_L_D.*vol/micg_2_kg;
morta = wq.WQ_DIAG_PHYTO_GREEN_MORT_MICG_L_D.*vol/micg_2_kg;
respi = wq.WQ_DIAG_PHYTO_GREEN_RESP_MICG_L_D.*vol/micg_2_kg; % Not independent of the above

% Mass sources converted to kg/d
produ = wq.WQ_DIAG_PHYTO_GREEN_PRIM_PROD_MICG_L_D.*vol/micg_2_kg;

% Limitation functions
light = wq.WQ_DIAG_PHYTO_GREEN_LGHT_LIM_ND;
tempt = wq.WQ_DIAG_PHYTO_GREEN_TMPTR_LIM_ND;
salin = wq.WQ_DIAG_PHYTO_GREEN_SAL_LIM_ND;
nitro = wq.WQ_DIAG_PHYTO_GREEN_N_LIM_ND;
phsph = wq.WQ_DIAG_PHYTO_GREEN_P_LIM_ND;
silic = wq.WQ_DIAG_PHYTO_GREEN_SI_LIM_ND;
for i = 1:size(light,1)
    for j = 1:size(light,2)
        lfmin(i,j) = min([light(i,j);tempt(i,j);salin(i,j);nitro(i,j);phsph(i,j);silic(i,j)]);
    end
end
temperature = wq.TEMP;

% Load base case temperature limitation
load tlim

%% Plotting
% Concentration and fluxes
figure(1)
set(gcf,'pos',[198,82,1372,857],'color','w')
s1 = subplot(3,1,1);
plot(wq.ResTime,phy(threeD_ID,:))
legend('Green phytoplankton concentration','location','southeast','numcolumns',1)
title(s1,'Simulated concentration'); ylabel(s1,'Concentration (\mu g/L)');xlabel(s1,'Time (hrs)')
grid on
% Sinks
s2 = subplot(3,1,2);
plot(wq.ResTime,excre(threeD_ID,:),'r')
hold on
plot(wq.ResTime,morta(threeD_ID,:))
plot(wq.ResTime,respi(threeD_ID,:))
legend('Excretion','Mortality','Respiration','location','southeast','numcolumns',3)
title(s2,'Sinks');ylabel(s2,'Flux (kg/day)')
set(gca,'ylim',ylims_neg); grid on
% Sources
s3 = subplot(3,1,3);
plot(wq.ResTime,produ(threeD_ID,:))
hold on
legend('Productivity','location','northeast','numcolumns',1)
title(s3,'Sources');xlabel(s3,'Time (hrs)');ylabel(s3,'Flux (kg/day)')
set(gca,'ylim',ylims); grid on

% Concentration and limitation functions
figure(2)
set(gcf,'pos',[128,42,1372,857],'color','w')
s1 = subplot(2,1,1);
plot(wq.ResTime,phy(threeD_ID,:))
legend('Green concentration','location','southeast','numcolumns',1)
title(s1,'Simulated concentration'); ylabel(s1,'Concentration (\mu g/L)');xlabel(s1,'Time (hrs)')
grid on
% Limitation functions
s2 = subplot(2,1,2);
plot(wq.ResTime,light(threeD_ID,:))
hold on
plot(wq.ResTime,tempt(threeD_ID,:))
plot(wq.ResTime,salin(threeD_ID,:))
plot(wq.ResTime,nitro(threeD_ID,:),'c')
plot(wq.ResTime,phsph(threeD_ID,:))
plot(wq.ResTime,silic(threeD_ID,:),'k')
plot(wq.ResTime,lfmin(threeD_ID,:),'k','linewidth',2)
set(gca,'ylim',ylims_lim_fun)

legend('Light','Temperature','Salinity','Nitrogen','Phosphorus','Silica','location','southeast','numcolumns',6)
title(s2,'Limitation functions');ylabel(s2,'Function')
grid on

% % Temperature limitation
% figure(3)
% set(gcf,'pos',[128,42,1372,857],'color','w')
% plot(base(:,1),base(:,3))
% title('Computed temperature function'); ylabel('Temperature function (-)');xlabel('Temperature (^oC)')
% grid on
% hold on
% plot(alt(:,1),alt(:,3))
% plot(temperature(threeD_ID,[2:size(light,2)]),tempt(threeD_ID,[2:size(light,2)]),'ro')
% legend('Base function','Modified function','Modified actual','location','southeast','numcolumns',1)





