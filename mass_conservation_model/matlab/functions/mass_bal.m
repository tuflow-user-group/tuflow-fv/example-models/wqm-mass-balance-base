function [mb,m] = mass_bal(mass,tol,varargin)

m(1:2,1) = mass(1);
for i = 3:length(mass) % Needed because there is no conc change between col 1 and 2 - synchronise outputs!
    flux_sum = 0.0;
    for j = 1:size(varargin,2)
        flux_sum = flux_sum + varargin{1,j}(i-1);
    end 
    m(i,1) = m(i-1,1) + flux_sum;
end

% compute percentage error
mb = (mass - m)*100./mass;

% remove mb's where mass is very small (DIV/0)
mb(mass<tol*max(mass)) = NaN; 

end
     