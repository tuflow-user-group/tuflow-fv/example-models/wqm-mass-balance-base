function [atm] = atmosph_flux(flux,NL,area)

[surf_cell_id] = surf_ids(NL);

atm(1:size(flux,2)) = 0;
for k = 1:size(flux,2)
    for i = 1:length(surf_cell_id)
        atm(k) = atm(k) + sum(area(i)*flux(surf_cell_id(i),k));
    end
end

end






