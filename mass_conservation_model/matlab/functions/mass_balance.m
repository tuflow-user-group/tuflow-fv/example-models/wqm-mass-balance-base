%% Mass balance script
% 09/02/2022
% Notes
% - Must use same output, wq dt and bc dt timesteps otherwise flux diagnostics are inconsistent with concs

%% Load data
netcdf_wq = netcdf_get_var([out_dir,run_ID,suffix]);

%% Parameters
load_parameters

%% Model geometry
area = netcdf_wq.cell_A;
NL = netcdf_wq.NL; zfaces = netcdf_wq.layerface_Z;

%% Array construction 
if contains(upper(run_ID), 'MGL')
    construct_arrays
else
    construct_arrays_MMM
end

%% WQ variable data
if contains(upper(run_ID), 'MGL')
    load_variables
else
    load_variables_MMM
end  

%% WQ diagnostic data
if contains(upper(run_ID), 'MGL')
    load_diagnostics
else
    load_diagnostics_MMM
end

%% Compute fluxes
if contains(upper(run_ID), 'MGL')
    compute_fluxes
else
    compute_fluxes_MMM
end

%% Mass balance
do_mass_balance

%% Write outputs
write_outputs

%% Plot
plot_mass_balance

%% Save
if save_all == 1
    clear netcdf_wq;
    exp3 = ['save ',out_dir,run_ID,'_mass_conservation']; eval(exp3)
end

%% Reload data
netcdf_wq = netcdf_get_var([out_dir,run_ID,'_WQ.nc']);

