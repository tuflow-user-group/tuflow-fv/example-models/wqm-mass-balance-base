%% Wiki: Use of Diagnostics
% An example matlab script to post process nitrate 
% concentrations in the mass conservation model 
% and assess flux pathways.
% This script is not intended for use other than directly with 
% the relevant wiki page discussion. The script should not be 
% altered in any way. Changes will not be supported
% TUFLOW 2023

clear all; close all;

%% User inputs
threeD_ID = 1; % 3D cell ID
threeD_ID_atm = 20; % Corresponding surface cell 3D ID
DIN_split = 0.5; % Split of DIN into ammonium and nitrate specified in 'atmospheric deposition == '
ylims = [0,80]; ylims_neg = [-80,0]; % ylimits for plotting
% Name an output file to process
% Change this manually as new results files with different WQ 
% parameterisations (described in the wiki page) are generated
wq = netcdf_get_var('..\..\results\MB_inorganics_MGL_WQ.nc'); % Base case WQ output file

% Do not change anything below here
%% Processing
% Conversions
m3_2_L = 1000.0; mg_2_kg = 1000000.0;

% Geometry
area = wq.cell_A;
thick = cell_dz(wq.NL,wq.layerface_Z);
for j = 1:length(wq.ResTime)
    for i = 1:length(wq.idx3)
        vol(wq.idx3(i):wq.idx3(i)+wq.NL(i)-1,j) = m3_2_L * thick(wq.idx3(i):wq.idx3(i)+wq.NL(i)-1,j) * area(i); %L
        are(wq.idx3(i):wq.idx3(i)+wq.NL(i)-1,j) = area(i); %m2
    end
end

% Time
wq.ResTime = wq.ResTime - wq.ResTime(1);

% Nitrate concentration
nit = wq.WQ_NITRATE_MG_L;

% Mass sinks converted to kg/d
denit = wq.WQ_DIAG_DENITRIFICATION_MG_L_D.*vol/mg_2_kg;
anamm = wq.WQ_DIAG_ANAER_NH4_OX_MG_L_D.*vol/mg_2_kg;
dnrao = wq.WQ_DIAG_DISS_NO3_RED_MG_L_D.*vol/mg_2_kg;
uptke = wq.WQ_DIAG_PHYTO_COM_NO3_UPTAKE_MG_L_D.*vol/mg_2_kg;

% Mass sources converted to kg/d
atmos = (wq.WQ_DIAG_DIN_ATMOS_EXCHANGE_MG_M2_D.*are/mg_2_kg) * DIN_split;
sdflx = wq.WQ_DIAG_ACTUAL_NO3_SED_FLUX_MG_M2_D/mg_2_kg;
nitrf = wq.WQ_DIAG_NITRIFICATION_MG_L_D.*vol/mg_2_kg;

%% Plotting
% Concentration
figure(1)
set(gcf,'pos',[198,82,1372,857],'color','w')
s1 = subplot(3,1,1);
plot(wq.ResTime,nit(threeD_ID,:))
legend('Nitrate concentration','location','southeast','numcolumns',1)
title(s1,'Simulated concentration'); ylabel(s1,'Concentration (mg/L)');xlabel(s1,'Time (hrs)')
grid on
% Sinks
s2 = subplot(3,1,2);
plot(wq.ResTime,-1*denit(threeD_ID,:))
hold on
plot(wq.ResTime,-1*anamm(threeD_ID,:))
plot(wq.ResTime,-1*dnrao(threeD_ID,:))
plot(wq.ResTime,-1*uptke(threeD_ID,:))
legend('Denitrification','Anammox','DRNA','uptake','location','southeast','numcolumns',4)
title(s2,'Sinks');ylabel(s2,'Flux (kg/day)')
set(gca,'ylim',ylims_neg); grid on
% Sources
s3 = subplot(3,1,3);
plot(wq.ResTime,atmos(threeD_ID_atm,:))
hold on
plot(wq.ResTime,sdflx(threeD_ID,:))
plot(wq.ResTime,nitrf(threeD_ID,:))
legend('Atmospheric deposition','Sediment flux','Nitrification','location','northeast','numcolumns',3)
title(s3,'Sources');xlabel(s3,'Time (hrs)');ylabel(s3,'Flux (kg/day)')
set(gca,'ylim',ylims); grid on


