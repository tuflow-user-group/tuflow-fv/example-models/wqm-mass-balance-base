% DO
[oxy_mass_bal,oxy_comp_mass] = mass_bal(oxy_mass, tol, ...                          
                                        oxy_atm_flux,oxy_sed_flux, ...                   
                                        oxy_nitrif_flux, ...                            
                                        oxy_phy_gpp_flux,oxy_phy_res_flux, ...          
                                        oxy_org_min_flux);   

% Pathogens
[pth_alive_attch_mass_bal,pth_alive_comp_mass] = mass_bal(pat_alat_mass, tol, ...   ! These diag fluxes include alive and atatched as reported                       
                                        pat_grw_flux,pat_alat_lgt_flux, ...                   
                                        pat_alat_mor_flux, ...                            
                                        pat_alat_al_sed_flux, pat_alat_at_sed_flux);  

[pth_dead_mass_bal,pth_dead_comp_mass] = mass_bal(pat_dea_mass, tol, ...   
                                        pat_dead_lgt_flux, ...                   
                                        pat_dead_mor_flux, ...                            
                                        pat_dead_de_sed_flux);  

% Inorganics
if sim_class > 0
    [sil_mass_bal,sil_comp_mass] = mass_bal(sil_mass,tol, ...
                                            sil_sed_flux, ...
                                            sil_phy_upt_flux,sil_phy_mor_flux,sil_phy_exc_flux);

    [amm_mass_bal,amm_comp_mass] = mass_bal(amm_mass,tol, ...
                                            amm_atm_flux,amm_sed_flux, ...
                                            amm_nitrif_flux, ...
                                            amm_anmmox_flux,amm_drna_flux, ...
                                            amm_phy_upt_flux, ...
                                            amm_phy_mor_flux,amm_phy_exc_flux, ...
                                            amm_org_min_flux, ...
                                            amm_org_pho_flux);
                                        
    [nit_mass_bal,nit_comp_mass] = mass_bal(nit_mass,tol, ...
                                            nit_atm_flux,nit_sed_flux, ...
                                            nit_nitrif_flux,nit_denit_flux, ...
                                            nit_anmmox_flux,nit_drna_flux, ...
                                            nit_phy_upt_flux, ...
                                            nit_org_min_flux);
                                        
    [frp_mass_bal,frp_comp_mass] = mass_bal(frp_mass,tol, ...
                                            frp_atm_flux,frp_sed_flux, ...
                                            frp_phy_upt_flux, ...
                                            frp_phy_mor_flux,frp_phy_exc_flux, ...
                                            frp_org_min_flux, ...
                                            frp_org_pho_flux);
                                        
    [phy_mass_bal,phy_comp_mass] = mass_bal(phy_mass,tol, ...
                                            phy_phy_sed_flux, ...
                                            phy_phy_upt_flux,phy_phy_res_flux,phy_phy_mor_flux,phy_phy_exc_flux);
end

% Labile organics
if sim_class > 1
    [poc_mass_bal,poc_comp_mass] = mass_bal(poc_mass,tol, ...
                                            poc_hyd_flux, ...             
                                            poc_phy_mor_flux, ...
                                            poc_bdn_flux, ...
                                            poc_sed_flux);
                                        
    [doc_mass_bal,doc_comp_mass] = mass_bal(doc_mass,tol, ...
                                            doc_sed_flux, ...
                                            doc_hyd_flux, ...             
                                            doc_min_flux, ...              
                                            doc_phy_exc_flux, ...
                                            doc_act_flux, ...
                                            doc_pho_flux);
                                        
    [pon_mass_bal,pon_comp_mass] = mass_bal(pon_mass,tol, ...
                                            pon_hyd_flux, ...             
                                            pon_phy_mor_flux, ...
                                            pon_bdn_flux, ...
                                            pon_sed_flux);
                                        
    [don_mass_bal,don_comp_mass] = mass_bal(don_mass,tol, ...
                                            don_sed_flux, ...
                                            don_hyd_flux, ...             
                                            don_min_flux, ...              
                                            don_phy_exc_flux, ...
                                            don_act_flux, ...
                                            don_pho_flux);
                                        
    [pop_mass_bal,pop_comp_mass] = mass_bal(pop_mass,tol, ...
                                            pop_hyd_flux, ...             
                                            pop_phy_mor_flux, ...
                                            pop_bdn_flux, ...
                                            pop_sed_flux);
                                        
    [dop_mass_bal,dop_comp_mass] = mass_bal(dop_mass,tol, ...
                                            dop_sed_flux, ...
                                            dop_hyd_flux, ...             
                                            dop_min_flux, ...              
                                            dop_phy_exc_flux, ...
                                            dop_act_flux, ...
                                            dop_pho_flux);
end

% Refractory organics
if sim_class > 2
    [rpc_mass_bal,rpc_comp_mass] = mass_bal(rpc_mass,tol, ...
                                            rpc_sed_flux, ...
                                            rpc_bdn_flux);

    [rdc_mass_bal,rdc_comp_mass] = mass_bal(rdc_mass,tol, ...
                                            rdc_act_flux, ...
                                            rdc_pho_flux);
                                        
    [rdn_mass_bal,rdn_comp_mass] = mass_bal(rdn_mass,tol, ...
                                            rdn_act_flux, ...
                                            rdn_pho_flux);
                                        
    [rdp_mass_bal,rdp_comp_mass] = mass_bal(rdp_mass,tol, ...
                                            rdp_act_flux, ...
                                            rdp_pho_flux);
end

% Totals 
% Pathogens
arr = length(tpt_mass);
tpt_mass_bal(1:arr-2,1) = (tpt_mass(2:arr-1) - tpt_mass(2)) * 100.0 / tpt_mass(2); 
tpt_mass_bal(arr-1:arr) = tpt_mass_bal(arr-2);
if sim_class > 0
    tni_mass_bal(1:arr-2,1) = (tni_mass(2:arr-1) - tni_mass(2)) * 100.0 / tni_mass(2); 
    tni_mass_bal(arr-1:arr) = tni_mass_bal(arr-2);
    tkn_mass_bal(1:arr-2,1) = (tkn_mass(2:arr-1) - tkn_mass(2)) * 100.0 / tkn_mass(2);
    tkn_mass_bal(arr-1:arr) = tkn_mass_bal(arr-2);
    tph_mass_bal(1:arr-2,1) = (tph_mass(2:arr-1) - tph_mass(2)) * 100.0 / tph_mass(2);
    tph_mass_bal(arr-1:arr) = tph_mass_bal(arr-2);
    toc_mass_bal(1:arr-2,1) = (toc_mass(2:arr-1) - toc_mass(2)) * 100.0 / toc_mass(2);
    toc_mass_bal(arr-1:arr) = toc_mass_bal(arr-2);
end  
