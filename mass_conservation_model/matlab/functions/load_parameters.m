% Constants
units = 1e6; secs_per_day = 86400; secs_per_hour = 3600;
L_per_m3 = 1000; ug_per_mg = 1000; Hundred_mL_per_L = 10.0; Xon = 2.0;
C_mm_mg = 12; O2_mm_mg = 32; N_mm_mg = 14; P_mm_mg = 31; Si_mm_mg = 28;

switch run_ID(1,4)
    case {'D','d'}
        sim_class = 0;
    case {'I','i'}
        sim_class = 1;
    case {'O','o'}
        sim_class = 3;
    otherwise
        display('Incorrect simulation class set. Options are: DO, inorganics or organics.')
        display('Exiting')
        return
end
% dt
dt = (netcdf_wq.ResTime(2) - netcdf_wq.ResTime(1)) * secs_per_hour;
% Anammox split
anammox_amm = 1.0; anammox_nit = 1.32;
% Conversions
conv_volu = dt * L_per_m3 / units / secs_per_day;
conv_volu_CFU =  dt * L_per_m3 / 1.000 / secs_per_day * Hundred_mL_per_L;
conv_area = dt * 1.000000 / units / secs_per_day;
conv_area_CFU = dt * 1.000000 / 1.000 / secs_per_day;
conv_mass = L_per_m3 / units;
conv_mass_CFU = L_per_m3 * Hundred_mL_per_L / 1.00;
% Plotting
lw = 2; dy = 0.10;