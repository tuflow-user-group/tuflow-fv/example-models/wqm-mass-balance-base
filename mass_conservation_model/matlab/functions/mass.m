function [m] = mass(con,NL,zl,area)

[dzs] = cell_dz(NL,zl);
conc = sum(con,3);

m(1:size(conc,2)) = 0;
for j = 1:size(conc,2)
    for i = 1:size(NL,1)
        start = sum(NL(1:i-1)) + 1;
        for k = start:(start + NL(i)) - 1
            m(j) = m(j) + sum(area(i)*(conc(k,j).*dzs(k,j)));
        end
    end
end


end
