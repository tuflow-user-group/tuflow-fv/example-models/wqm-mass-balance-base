%DO 
oxy_atm = netcdf_wq.WQ_DIAG_O2_ATMOS_EXCHANGE_MG_M2_D;
oxy_sed = netcdf_wq.WQ_DIAG_ACTUAL_O2_SED_FLUX_MG_M2_D;

% get path names and positions
[pat_tot_names,pat_grw_names,pat_lgt_names,pat_mor_names, ...
            pat_als_names,pat_des_names, ...
            pat_ats_names,pat_atm_names] = pat_field_names(nc_names);
    att_count = 0;
    % Pathogen calculations
    for i = 1:num_pth
        exp = ['pth_tot(:,:,i) = 1.0 * netcdf_wq.',pat_tot_names{i,:},';']; eval(exp); 
        exp = ['pth_grw(:,:,i) = 1.0 * netcdf_wq.',pat_grw_names{i,:},';']; eval(exp); 
        exp = ['pth_lgt_al(:,:,i) = 1.0 * netcdf_wq.',pat_lgt_names{i,:},';']; eval(exp); 
        exp = ['pth_lgt_de(:,:,i) = -1.0 * netcdf_wq.',pat_lgt_names{i,:},';']; eval(exp); 
        exp = ['pth_mor_al(:,:,i) = 1.0 * netcdf_wq.',pat_mor_names{i,:},';']; eval(exp); 
        exp = ['pth_mor_de(:,:,i) = -1.0 * netcdf_wq.',pat_mor_names{i,:},';']; eval(exp); 
        exp = ['pth_als(:,:,i) = 1.0 * netcdf_wq.',pat_als_names{i,:},' * Hundred_mL_per_L * L_per_m3 .* cell_dz(NL,zfaces);']; eval(exp); 
        exp = ['pth_des(:,:,i) = 1.0 * netcdf_wq.',pat_des_names{i,:},' * Hundred_mL_per_L * L_per_m3 .* cell_dz(NL,zfaces);']; eval(exp); 
        if pth_model(i) ~= 0
            att_count = att_count + 1;
            exp = ['pth_ats(:,:,i) = 1.0 * netcdf_wq.',pat_ats_names{att_count,:},' * Hundred_mL_per_L * L_per_m3 .* cell_dz(NL,zfaces);']; eval(exp); 
            exp = ['pth_atm(:,:,i) = 1.0 * netcdf_wq.',pat_atm_names{att_count,:},';']; eval(exp); 
        end
    end

% Inorganics
if sim_class > 0
    % get names and positions
    [phc_sed_names,phn_sed_names,php_sed_names, ...
        phy_gpp_names,phy_res_names, ...
        phy_exc_names,phy_exn_names,phy_exp_names, ... 
        phy_moc_names,phy_mon_names,phy_mop_names] = phy_field_names(nc_names);
    % Atmosphere
    amm_atm = netcdf_wq.WQ_DIAG_DIN_ATMOS_EXCHANGE_MG_M2_D * (1.0 - amm_nit_atm_split); 
    nit_atm = netcdf_wq.WQ_DIAG_DIN_ATMOS_EXCHANGE_MG_M2_D * (amm_nit_atm_split);
    frp_atm = netcdf_wq.WQ_DIAG_DIP_ATMOS_EXCHANGE_MG_M2_D;
    % Sediment
    sil_sed = netcdf_wq.WQ_DIAG_ACTUAL_SI_SED_FLUX_MG_M2_D;
    amm_sed = netcdf_wq.WQ_DIAG_ACTUAL_NH4_SED_FLUX_MG_M2_D;
    nit_sed = netcdf_wq.WQ_DIAG_ACTUAL_NO3_SED_FLUX_MG_M2_D;
    frp_sed = netcdf_wq.WQ_DIAG_ACTUAL_FRP_SED_FLUX_MG_M2_D;
    % Nitrification
    amm_nit = -1 * netcdf_wq.WQ_DIAG_NITRIFICATION_MG_L_D;
    oxy_nit = amm_nit * (1/N_mm_mg) * O2_mm_mg * Xon;
    nit_nit = -1 * amm_nit;
    % Inorganic denitrification
    nit_den = -1 * netcdf_wq.WQ_DIAG_DENITRIFICATION_MG_L_D;
    % Inorganic anaerobic ammonium oxidation
    annamox = -1 * netcdf_wq.WQ_DIAG_ANAER_NH4_OX_MG_L_D;
    R = (anammox_amm + anammox_nit) * annamox ./ (anammox_amm*amm + anammox_nit*nit);
    amm_amx = R * anammox_amm .* amm / (anammox_amm + anammox_nit);
    nit_amx = R * anammox_nit .* nit / (anammox_amm + anammox_nit);
    % Inorganic dissim reduction of nitrate to ammonium
    amm_drn = netcdf_wq.WQ_DIAG_DISS_NO3_RED_MG_L_D;
    nit_drn = -1 * amm_drn;
    % Phytoplankton uptake
    % Nitrogen
    nit_upt = -1 * netcdf_wq.WQ_DIAG_PHYTO_COM_NO3_UPTAKE_MG_L_D;
    amm_upt = -1 * netcdf_wq.WQ_DIAG_PHYTO_COM_NH4_UPTAKE_MG_L_D;
    n2g_upt = +1 * netcdf_wq.WQ_DIAG_PHYTO_COM_N2_UPTAKE_MG_L_D;
    % Phosphorus
    frp_upt = -1 * netcdf_wq.WQ_DIAG_PHYTO_COM_P_UPTAKE_MG_L_D;
    % Phytoplankton group calculations
    for i = 1:num_phy
        % Phytoplankton sedimentation
        exp = ['phc_sed(:,:,i) = 1.0 * netcdf_wq.',phc_sed_names{i,:},' * L_per_m3 * X_cc(i) .* cell_dz(NL,zfaces) / ug_per_mg;']; eval(exp); % Carbon 
        exp = ['phn_sed(:,:,i) = 1.0 * netcdf_wq.',phn_sed_names{i,:},' * L_per_m3 .* cell_dz(NL,zfaces) ;']; eval(exp); % Nitrogen 
        exp = ['php_sed(:,:,i) = 1.0 * netcdf_wq.',php_sed_names{i,:},' * L_per_m3 .* cell_dz(NL,zfaces) ;']; eval(exp); % Phosphorus 
        % Phytoplankton uptake
        exp = ['car_gpp(:,:,i) = +1 * netcdf_wq.',phy_gpp_names{i,:},' * X_cc(i) / ug_per_mg;']; eval(exp); % Carbon
        oxy_gpp(:,:,i) = +1 * car_gpp(:,:,i) *(1/C_mm_mg)*O2_mm_mg; % Oxygen
        sil_upt(:,:,i) = -1 * car_gpp(:,:,i) * X_scon(i) / X_cc(i); % Silicate
        % Phytoplankton respiration
        exp = ['car_rsf(:,:,i) = +1 * netcdf_wq.',phy_res_names{i,:},' * X_cc(i) / ug_per_mg;']; eval(exp); % Carbon
        oxy_rsf(:,:,i) = +1 * car_rsf(:,:,i) * (1/C_mm_mg)*O2_mm_mg; % Oxygen
        % Phytoplankton excretion        
        exp = ['car_exc(:,:,i) = +1 * netcdf_wq.',phy_exc_names{i,:},' * X_cc(i) / ug_per_mg;']; eval(exp); % Carbon           
        exp = ['amm_exc(:,:,i) = -1 * netcdf_wq.',phy_exn_names{i,:},';']; eval(exp); % Nitrogen       
        exp = ['frp_exc(:,:,i) = -1 * netcdf_wq.',phy_exp_names{i,:},';']; eval(exp); % Phosphorus        
        sil_exc(:,:,i) = ((-1 * car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * K_fdom(i) * (1.0 -   0.00) + ... 
                     (car_gpp(:,:,i) ./ phy(:,:,i)) * f_pr(i)) * (X_scon(i) / X_cc(i)) .* phy(:,:,i); % Silicate
        % Phytoplankton mortality
        exp = ['car_mor(:,:,i) = +1 * netcdf_wq.',phy_moc_names{i,:},' * X_cc(i) / ug_per_mg;']; eval(exp); % Carbon
        exp = ['amm_mor(:,:,i) = -1 * netcdf_wq.',phy_mon_names{i,:},';']; eval(exp); % Nitrogen
        exp = ['frp_mor(:,:,i) = -1 * netcdf_wq.',phy_mop_names{i,:},';']; eval(exp); % Phosphorus
        sil_mor(:,:,i) = -1 * (car_rsf(:,:,i) / k_fres(i) ./ phy(:,:,i)) * (1.0 - K_fdom(i)) * ...         
                              (X_scon(i) / X_cc(i)) .* phy(:,:,i); % Silicate
    end  
end

% Labile organics
if sim_class > 1
    % Sediment
    doc_sed = netcdf_wq.WQ_DIAG_ACTUAL_DOC_SED_FLUX_MG_M2_D;
    don_sed = netcdf_wq.WQ_DIAG_ACTUAL_DON_SED_FLUX_MG_M2_D;
    dop_sed = netcdf_wq.WQ_DIAG_ACTUAL_DOP_SED_FLUX_MG_M2_D;
    doc_hyd = netcdf_wq.WQ_DIAG_POC_HYDROL_MG_L_D;
    don_hyd = netcdf_wq.WQ_DIAG_PON_HYDROL_MG_L_D;
    dop_hyd = netcdf_wq.WQ_DIAG_POP_HYDROL_MG_L_D;
    % Settling
    poc_sed = netcdf_wq.WQ_DIAG_POC_SEDMTN_FLUX_MG_L_D * L_per_m3 .* cell_dz(NL,zfaces);
    pon_sed = netcdf_wq.WQ_DIAG_PON_SEDMTN_FLUX_MG_L_D * L_per_m3 .* cell_dz(NL,zfaces);
    pop_sed = netcdf_wq.WQ_DIAG_POP_SEDMTN_FLUX_MG_L_D * L_per_m3 .* cell_dz(NL,zfaces);
    % Hydrolysis
    poc_hyd = -1 * netcdf_wq.WQ_DIAG_POC_HYDROL_MG_L_D;
    pon_hyd = -1 * netcdf_wq.WQ_DIAG_PON_HYDROL_MG_L_D;
    pop_hyd = -1 * netcdf_wq.WQ_DIAG_POP_HYDROL_MG_L_D;
    % Organic mineralisation organic sinks
    doc_min = -1 * netcdf_wq.WQ_DIAG_DOC_MINERL_MG_L_D;          
    don_min = -1 * netcdf_wq.WQ_DIAG_DON_MINERL_MG_L_D;         
    dop_min = -1 * netcdf_wq.WQ_DIAG_DOP_MINERL_MG_L_D;        
    % Excretion
    doc_exc = -1 * car_exc; don_exc = amm_exc; dop_exc = frp_exc;
    amm_exc = zero_init; frp_exc = zero_init;
    % Mortality
    poc_mor = -1 * car_mor; pon_mor = amm_mor; pop_mor = frp_mor;
    amm_mor = zero_init; frp_mor = zero_init;
    % Organic mineralisation inorganic sinks
    oxy_omi = -1 * (netcdf_wq.WQ_DIAG_DOC_MINERL_MG_L_D - ...
              netcdf_wq.WQ_DIAG_DOC_MINERL_DENIT_MG_L_D - ...
              netcdf_wq.WQ_DIAG_DOC_MINERL_AN_MG_L_D) * (1/C_mm_mg)*O2_mm_mg;
    nit_omi = -1 * netcdf_wq.WQ_DIAG_DOC_MINERL_DENIT_MG_L_D * (1/C_mm_mg)*N_mm_mg;
    % Organic mineralisation inorganic sources
    amm_omi = -1 * don_min; frp_omi = -1 * dop_min;
    % RPOM breakdown
    poc_bdn = zero_init; pon_bdn = zero_init; pop_bdn = zero_init;
    % Refractory activation
    doc_act = zero_init; don_act = zero_init; dop_act = zero_init;
    % Refractory photolysis
    doc_pho = zero_init; don_pho = zero_init; dop_pho = zero_init;
end

% Refractory organics
if sim_class > 2
    % Settling
    rpc_sed = netcdf_wq.WQ_DIAG_RPOM_SEDMTN_FLUX_MG_L_D * L_per_m3 .* cell_dz(NL,zfaces);
    % RPOM breakdown
    rpc_bdn = -1 * netcdf_wq.WQ_DIAG_RPOM_BREAKDOWN_MG_L_D;
    poc_bdn = -1 * rpc_bdn; 
    pon_bdn = -1 * rpc_bdn * X_nrpom * (1/C_mm_mg) * N_mm_mg;
    pop_bdn = -1 * rpc_bdn * X_prpom * (1/C_mm_mg) * P_mm_mg;
    % Refractory activation
    rdc_act = -1 * netcdf_wq.WQ_DIAG_RDOC_ACTV_MG_L_D;
    rdn_act = -1 * netcdf_wq.WQ_DIAG_RDON_ACTV_MG_L_D;
    rdp_act = -1 * netcdf_wq.WQ_DIAG_RDOP_ACTV_MG_L_D;
    doc_act = -1 * rdc_act; 
    don_act = -1 * rdn_act; 
    dop_act = -1 * rdp_act; 
    % Photolysis
    try 
       rdc_pho = -1 * netcdf_wq.WQ_DIAG_PHOTOLYSIS_MG_L_D;
    catch exception
       rdc_pho = 0.0 * dop_act;
    end
    rdn_pho = rdc_pho .* (rdn./rdc);
    rdp_pho = rdc_pho .* (rdp./rdc);
    
    doc_pho = -1 * rdc_pho * f_pho;
    don_pho = -1 * rdn_pho * f_pho;
    dop_pho = -1 * rdp_pho * f_pho;
    dic_pho = -1 * rdc_pho * (1.0 - f_pho);
    amm_pho = -1 * rdn_pho * (1.0 - f_pho);
    frp_pho = -1 * rdp_pho * (1.0 - f_pho);
end
