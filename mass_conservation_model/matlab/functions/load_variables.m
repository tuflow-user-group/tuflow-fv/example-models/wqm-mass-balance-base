% DO
oxy = netcdf_wq.WQ_DISS_OXYGEN_MG_L;

% Pathogens
for i = 1:num_pth
    exp = ['pth_ali(:,:,i) = netcdf_wq.',pth_names_alive{i,1},';']; eval(exp); 
    exp = ['pth_dea(:,:,i) = netcdf_wq.',pth_names_dead{i,1},';']; eval(exp); 
end

% Inorganics
if sim_class > 0
    sil = netcdf_wq.WQ_SILICATE_MG_L;
    amm = netcdf_wq.WQ_AMMONIUM_MG_L; nit = netcdf_wq.WQ_NITRATE_MG_L;
    frp = netcdf_wq.WQ_FRP_MG_L;
    try 
       fra = netcdf_wq.WQ_FRP_ADS_MG_L;
    catch exception
       fra = 0.0 * frp;
    end
    % phytos
    for i = 1:num_phy
        exp = ['phy(:,:,i) = netcdf_wq.',phy_names{i,1},' * X_cc(i) / ug_per_mg;']; eval(exp); 
    end
end

% Labile organics
if sim_class > 1
    doc = netcdf_wq.WQ_DOC_MG_L; poc = netcdf_wq.WQ_POC_MG_L;
    don = netcdf_wq.WQ_DON_MG_L; pon = netcdf_wq.WQ_PON_MG_L;
    dop = netcdf_wq.WQ_DOP_MG_L; pop = netcdf_wq.WQ_POP_MG_L;
end

% Refractory organics
if sim_class > 2
    rdc = netcdf_wq.WQ_RDOC_MG_L; 
    rdn = netcdf_wq.WQ_RDON_MG_L; 
    rdp = netcdf_wq.WQ_RDOP_MG_L; 
    rpc = netcdf_wq.WQ_RPOM_MG_L; 
end