% DO
oxy = netcdf_wq.WQ_DISS_OXYGEN_MM_M3/L_per_m3*O2_mm_mg;

% Pathogens
for i = 1:num_pth
    exp = ['pth_ali(:,:,i) = netcdf_wq.',pth_names_alive{i,1},'/L_per_m3/10.0;']; eval(exp); 
    exp = ['pth_dea(:,:,i) = netcdf_wq.',pth_names_dead{i,1},'/L_per_m3/10.0;']; eval(exp); 
end

% Inorganics
if sim_class > 0
    sil = netcdf_wq.WQ_SILICATE_MM_M3/L_per_m3*28;
    amm = netcdf_wq.WQ_AMMONIUM_MM_M3/L_per_m3*N_mm_mg; nit = netcdf_wq.WQ_NITRATE_MM_M3/L_per_m3*N_mm_mg;
    frp = netcdf_wq.WQ_FRP_MM_M3/L_per_m3*P_mm_mg;
    try 
       fra = netcdf_wq.WQ_FRP_ADS_MM_M3/L_per_m3*P_mm_mg;
    catch exception
       fra = 0.0 * frp;
    end
    % phytos
    for i = 1:num_phy
        exp = ['phy(:,:,i) = netcdf_wq.',phy_names{i,1},' * X_cc(i) / ug_per_mg;']; eval(exp); 
        phy(:,:,i) = phy(:,:,i)*C_mm_mg/X_cc(i);
    end
end

% Labile organics
if sim_class > 1
    doc = netcdf_wq.WQ_DOC_MM_M3/L_per_m3*C_mm_mg; poc = netcdf_wq.WQ_POC_MM_M3/L_per_m3*C_mm_mg;
    don = netcdf_wq.WQ_DON_MM_M3/L_per_m3*N_mm_mg; pon = netcdf_wq.WQ_PON_MM_M3/L_per_m3*N_mm_mg;
    dop = netcdf_wq.WQ_DOP_MM_M3/L_per_m3*P_mm_mg; pop = netcdf_wq.WQ_POP_MM_M3/L_per_m3*P_mm_mg;
end

% Refractory organics
if sim_class > 2
    rdc = netcdf_wq.WQ_RDOC_MM_M3/L_per_m3*C_mm_mg; 
    rdn = netcdf_wq.WQ_RDON_MM_M3/L_per_m3*N_mm_mg; 
    rdp = netcdf_wq.WQ_RDOP_MM_M3/L_per_m3*P_mm_mg; 
    rpc = netcdf_wq.WQ_RPOM_MM_M3/L_per_m3*C_mm_mg; 
end