function [ids] = surf_ids(NL)

ids(1) = NL(1);
for i = 2:length(NL)
    ids(i) = ids(i-1) + NL(i);
end

end
