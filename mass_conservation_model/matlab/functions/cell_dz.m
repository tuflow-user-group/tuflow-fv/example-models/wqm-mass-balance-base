function [dz] = cell_dz(NL,zl)

NL_start(1) = 1;
NL_end(1) = NL_start(1) + NL(1);

for i = 2:length(NL)
    NL_start(i) = NL_start(i-1) + NL(i-1) + 1;
    NL_end(i) = NL_start(i) + NL(i);
end

for k = 1:size(zl,2)
    for i = 1:length(NL)
        for j = NL_start(i):NL_end(i) - 1
            dz(j-(i-1),k) = zl(j+1,k) - zl(j,k);
        end
    end
end

dz = -dz;

end
        


        