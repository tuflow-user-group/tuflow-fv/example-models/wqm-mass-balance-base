@echo off
REM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
REM Batch file to run mass balance model
REM Do not alter
REM TUFLOW 2022
REM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

setlocal EnableDelayedExpansion
REM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
REM Convert input arguments to capitals

for %%a in (%1%) do (
   set "arg1=%%a"
   for %%b in (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) do (
      set "arg1=!arg1:%%b=%%b!"
   )
)

for %%a in (%2%) do (
   set "arg2=%%a"
   for %%b in (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) do (
      set "arg2=!arg2:%%b=%%b!"
   )
)
REM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
REM Check to make sure arguments are valid

set arg1OK=F
if %arg1%==DO set arg1OK=T
if %arg1%==INORGANICS set arg1OK=T
if %arg1%==ORGANICS set arg1OK=T
set arg2OK=F
if %arg2%==MGL set arg2OK=T
if %arg2%==MMM set arg2OK=T

if %arg1OK%==F (
    echo Incorrect first argument %arg1%
    )
if %arg2OK%==F (
    echo Incorrect second argument %arg2%
    )
REM ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
REM Run model

if %arg2%==MGL (
    if %arg1%==DO (
        echo Running DO MGL simulation...
        cd runs
        %3% MB_DO_MGL.fvc
        cd ..\
        )

    if %arg1%==INORGANICS (
        echo Running INORGANICS MGL simulation...
        cd runs
        %3% MB_inorganics_MGL.fvc
        cd ..\
        )

    if %arg1%==ORGANICS (
        echo Running ORGANICS MGL simulation...
        cd runs
        %3% MB_organics_MGL.fvc
        cd ..\
        )
    )

if %arg2%==MMM (
    if %arg1%==DO (
        echo Running DO MMM simulation...
        cd runs
        %3% MB_DO_MMM.fvc
        cd ..\
        )

    if %arg1%==INORGANICS (
        echo Running INORGANICS MMM simulation...
        cd runs
        %3% MB_inorganics_MMM.fvc
        cd ..\
        )

    if %arg1%==ORGANICS (
        echo Running ORGANICS MMM simulation...
        cd runs
        %3% MB_organics_MMM.fvc
        cd ..\
        )
    )
